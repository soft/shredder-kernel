#include <ctype.h>
#include "main.h"

void onError(const char *str) {
    write(2, str, strlen(str));
    write(2, "\n", 1);
}

int writeZero(int fd) {
    for (int i = 0; i < ROSA_BLOCK; ++i) {
        if (write(fd, "\0", 1) < 0) {
            onError("Error while writing randomize");
            return -1;
        }
    }
    return 0;
}

int writeOne(int fd) {
    for (int i = 0; i < ROSA_BLOCK; ++i) {
        if (write(fd, "\1", 1) < 0) {
            onError("Error while writing randomize");
            return -1;
        }
    }
    return 0;
}

int writeZeroOne(int fd, unsigned count_size, int loops) {
    for (int i = 0; i < loops; ++i) {
        lseek(fd, 0, SEEK_SET);
        for (unsigned j = 0; j < count_size; ++j) {
            if (writeZero(fd) < 0) {
                return -1;
            }
        }
        fsync(fd);
        lseek(fd, 0, SEEK_SET);
        for (unsigned j = 0; j < count_size; ++j) {
            if (writeOne(fd) < 0) {
                return -1;
            }
        }
        fsync(fd);
    }
    return 0;
}

int writeRandom(int fd, unsigned count_size) {
    int randDevice = open("/dev/urandom", O_RDONLY);
    if (randDevice < 0) {
        onError("Failed to open rand device");
        return -1;
    }

    char buf[ROSA_BLOCK];
    for (unsigned i = 0; i < count_size; ++i) {
        read(randDevice, buf, ROSA_BLOCK);
        if (write(fd, buf, ROSA_BLOCK) < 0) {
            onError("Error while writing randomize");
            return -1;
        }
    }
    return 0;
}

int writeRandomNTimes(int fd, unsigned count_size, int loops) {
    for (int i = 0; i < loops; ++i) {
        lseek(fd, 0, SEEK_SET);
        if (writeRandom(fd, count_size) < 0) {
            return -1;
        }
        fsync(fd);
    }
    return 0;
}

int main(int ac, char **av) {
    if (ac < 5) {
        onError("Usage: rosa-shredder-user [path] [uid]");
        return (0);
    }

    unsigned int uid = atoi(av[2]);
    if (setuid(uid) != 0) {
        onError("Failed to setup UID");
        return (1);
    }

    int fd = open(av[1], O_RDWR);

    if (fd >= 0) {
        unsigned count_size = 1; // cause just one time we should write
        struct stat stat_buf;
        if (stat(av[1], &stat_buf) == 0) {
            count_size += stat_buf.st_size / ROSA_BLOCK;
            if (strstr(av[3], "random")) {
                writeRandomNTimes(fd, count_size, atoi(av[4]));
            } else if (strstr(av[3], "zeroone")) {
                writeZeroOne(fd, count_size, atoi(av[4]));
            } else {
                onError("Undefined mode");
            }
        } else {
            onError("Failed to get file's size");
        }
        close(fd);
    } else {
        onError("Failed to open file");
        return (1);
    }

    return (0);
}