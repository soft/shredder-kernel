/* *************************************************************************************************** */
/*                                                                                                     */
/*                                                          :::::::::   ::::::::   ::::::::      :::   */
/*  own_mkdir.c                                            :+:    :+: :+:    :+: :+:    :+:   :+: :+:  */
/*                                                        +:+    +:+ +:+    +:+ +:+         +:+   +:+  */
/*  By: Ivan Marochkin <i.marochkin@rosalinux.ru>        +#++:++#:  +#+    +:+ +#++:++#++ +#++:++#++:  */
/*                                                      +#+    +#+ +#+    +#+        +#+ +#+     +#+   */
/*  Created: 2019/12/02 16:40:04 by Ivan Marochkin     #+#    #+# #+#    #+# #+#    #+# #+#     #+#    */
/*  Updated: 2019/12/02 16:40:04 by Ivan Marochkin    ###    ###  ########   ########  ###     ###     */
/*                                                                                                     */
/* *************************************************************************************************** */

#include "shredder-kernel.h"

static char *mode = "none";
static char *loops = "0";

module_param(mode, charp, 0660);
module_param(loops, charp, 0660);

static uint64_t get_cr0(void) {

    uint64_t ret;
    __asm__ volatile (
        "movq %%cr0, %[ret]"
        : [ret] "=r" (ret)
    );
    return (ret);
}

static void set_cr0(uint64_t cr0) {

    __asm__ volatile (
        "movq %[cr0], %%cr0"
        :
        : [cr0] "r" (cr0)
    );
}

static char *duplicate_filename(const char __user *filename) {

	char *kernel_filename;
	kernel_filename = kmalloc(ROSA_BSIZE, GFP_KERNEL);
	if (!kernel_filename) {
		return (NULL);
	}
	if (strncpy_from_user(kernel_filename, filename, ROSA_BSIZE) < 0) {
		kfree(kernel_filename);
		return (NULL);
	}
	return (kernel_filename);
}

static int do_shred_sync(char *path, unsigned int uid) {
	
	int res_shred = 0, res_sync = 0;
	char *argv[] = {"rosa-shredder-user", NULL, NULL, mode, loops, NULL};
	char str_uid[16] = {0};
	char *envp[] = {NULL};
	argv[1] = path;
	sprintf(str_uid, "%u", uid);
	argv[2] = str_uid;
	res_shred = call_usermodehelper("/usr/sbin/rosa-shredder-user", argv, envp, 1);
	if (res_shred) {
		printk(KERN_ERR "shredder-kernel: call_usermodehelper error %s\n", argv[0]);
	}
	// else {
	// 	printk(KERN_INFO "shredder-kernel: shreded %s\n", argv[1]);
	// }
	argv[0] = "sync";
	argv[1] = NULL;
	res_sync = call_usermodehelper("/bin/sync", argv, envp, 1);
	if (res_sync) {
		printk(KERN_ERR "shredder-kernel: call_usermodehelper error %s\n", argv[0]);
	}
	return (res_shred + res_sync);
}

static asmlinkage long own_sys_unlinkat(struct pt_regs *regs) {

	int err;
	struct filename *filename_struct = 0;
	struct path file_path;
	char *buff;
	char *filename_char;
	char *full_name = 0;

	// const struct cred *cred = current_cred();
	// printk("uid: %u\n", current_uid().val);
	// printk("suid: %ud\n", cred->suid.val);
	// printk("euid: %u\n", current_euid().val);
	filename_char = duplicate_filename((void *) regs->si);
	if (!filename_char) {
		printk(KERN_ERR "shredder-kernel: kmalloc error\n");
		return (orig_sys_unlinkat(regs));
	}

	if (regs->di == 4294967196) {
		err = do_shred_sync(filename_char, current_uid().val);
	} else {
		buff = kmalloc(ROSA_BSIZE, GFP_KERNEL);
		if (!buff) {
			kfree(filename_char);
			printk(KERN_ERR "shredder-kernel: kmalloc error\n");
			return (orig_sys_unlinkat(regs));
		}
		memset(buff, 0, ROSA_BSIZE);
		filename_struct = orig_getname_kernel(filename_char);
		if (!filename_struct) {
			kfree(filename_char);
			printk(KERN_ERR "shredder-kernel: can't create filename_struct\n");
			return (orig_sys_unlinkat(regs));
		}
		err = orig_filename_lookup(regs->di, filename_struct, 0, &file_path, NULL);
		if (!err) {
			full_name = orig_dentry_path(file_path.dentry, buff, ROSA_BSIZE);
			if (full_name) {
				err = do_shred_sync(full_name, current_uid().val);
			} else {
				printk(KERN_ERR "shredder-kernel: dentry_path returns error\n");
			}
		} else {
			printk(KERN_ERR "shredder-kernel: filename_lookup returns error\n");
		}
		kfree(buff);
	}
	kfree(filename_char);
	return (orig_sys_unlinkat(regs));
}

static asmlinkage long own_sys_unlink(struct pt_regs *regs)
{
	char *res = 0;
	long ret, err;
	char *kernel_filename;

	kernel_filename = duplicate_filename((void*)regs->di);

	res = strnstr(kernel_filename, "/dev/", 5);

    if (!res) {
		err = do_shred_sync(kernel_filename, current_uid().val);
    }

	kfree(kernel_filename);

	ret = orig_sys_unlink(regs);

	return ret;
}

static int __init rosa_shredder_init(void) {

    void **sct;
    set_cr0(get_cr0() & ~CR0_WRITE_PROTECT);
    sct = (void**)kallsyms_lookup_name("sys_call_table");
	orig_getname_kernel = (void *)kallsyms_lookup_name("getname_kernel");
	orig_filename_lookup = (void *)kallsyms_lookup_name("filename_lookup");
	orig_dentry_path = (void *)kallsyms_lookup_name("dentry_path");
    orig_sys_unlink = sct[__NR_unlink];
    sct[__NR_unlink] = own_sys_unlink;
    orig_sys_unlinkat = sct[__NR_unlinkat];
    sct[__NR_unlinkat] = own_sys_unlinkat;
    set_cr0(get_cr0() | CR0_WRITE_PROTECT);
	/* insert check */

	printk(KERN_ERR "Kernel-shedder runs with: %s, %s\n", mode, loops);

	return 0;
}

static void __exit rosa_shredder_exit(void) {

    void **sct;
    set_cr0(get_cr0() & ~CR0_WRITE_PROTECT);
    sct = (void**)kallsyms_lookup_name("sys_call_table");
    sct[__NR_unlink] = orig_sys_unlink;
    sct[__NR_unlinkat] = orig_sys_unlinkat;
    set_cr0(get_cr0() | CR0_WRITE_PROTECT);
}

module_init(rosa_shredder_init)
module_exit(rosa_shredder_exit)
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ivan Marochkin <i.marochkin@rosalinux.ru>" );
MODULE_VERSION("1");
