
#ifndef __SHREDDER_KERNEL_H__
#define __SHREDDER_KERNEL_H__

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>
#include <linux/moduleparam.h>
#include <linux/unistd.h>
#include <linux/init.h>
#include <linux/kallsyms.h>
#include <linux/syscalls.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/cred.h>
#include <linux/fs_struct.h>
#include <linux/path.h>
#include <linux/dcache.h>
#include <linux/string.h>
#include <linux/fdtable.h>
#include <linux/set_memory.h>
#include <asm/cacheflush.h>
#include <asm/processor-flags.h>
#include <asm/ptrace.h>
#include <asm/syscall.h>
#include <asm/ptrace.h>
#include <asm/ptrace-abi.h>
#include <asm/bug.h>
#include <asm/kdebug.h>
#include <linux/sched/debug.h>
#include <linux/namei.h>

#define CR0_WRITE_PROTECT   (1 << 16)
#define ROSA_BSIZE          4096

static asmlinkage long (* orig_sys_unlinkat)(struct pt_regs *regs);
static asmlinkage long (* orig_sys_unlink)(struct pt_regs *regs);

static asmlinkage struct filename * (* orig_getname_kernel)(const char *filename);
static asmlinkage int (* orig_filename_lookup)(int dfd, struct filename *name, unsigned flags, struct path *path, struct path *root);
static asmlinkage char * (* orig_dentry_path)(struct dentry *, char *, int);

#endif /* __SHREDDER_KERNEL_H__ */