/*
 * Hooking unlink and unlinkat functions using ftrace framework
 *
 * Based on: ilammy, https://github.com/ilammy/ftrace-hook
 */

#define pr_fmt(fmt) "ftrace_hook: " fmt

#include <linux/ftrace.h>
#include <linux/kallsyms.h>
#include <linux/kernel.h>
#include <linux/linkage.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/version.h>
#include <linux/namei.h>
#include <linux/path.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/fs_struct.h>

MODULE_DESCRIPTION("Hooking unlink and unlinkat functions using ftrace framework");
MODULE_AUTHOR("I.Marochkin <i.marochkin@rosalinux.ru>");
MODULE_LICENSE("GPL");

#define USE_FENTRY_OFFSET 0

struct ftrace_hook {
	const char *name;
	void *function;
	void *original;

	unsigned long address;
	struct ftrace_ops ops;
};

static int fh_resolve_hook_address(struct ftrace_hook *hook)
{
	hook->address = kallsyms_lookup_name(hook->name);

	if (!hook->address) {
		pr_debug("unresolved symbol: %s\n", hook->name);
		return -ENOENT;
	}

#if USE_FENTRY_OFFSET
	*((unsigned long*) hook->original) = hook->address + MCOUNT_INSN_SIZE;
#else
	*((unsigned long*) hook->original) = hook->address;
#endif

	return 0;
}

static void notrace fh_ftrace_thunk(unsigned long ip, unsigned long parent_ip,
		struct ftrace_ops *ops, struct pt_regs *regs)
{
	struct ftrace_hook *hook = container_of(ops, struct ftrace_hook, ops);

#if USE_FENTRY_OFFSET
	regs->ip = (unsigned long) hook->function;
#else
	if (!within_module(parent_ip, THIS_MODULE))
		regs->ip = (unsigned long) hook->function;
#endif
}

int fh_install_hook(struct ftrace_hook *hook)
{
	int err;

	err = fh_resolve_hook_address(hook);
	if (err)
		return err;
	hook->ops.func = fh_ftrace_thunk;
	hook->ops.flags = FTRACE_OPS_FL_SAVE_REGS
	                | FTRACE_OPS_FL_RECURSION_SAFE
	                | FTRACE_OPS_FL_IPMODIFY;

	err = ftrace_set_filter_ip(&hook->ops, hook->address, 0, 0);
	if (err) {
		pr_debug("ftrace_set_filter_ip() failed: %d\n", err);
		return err;
	}

	err = register_ftrace_function(&hook->ops);
	if (err) {
		pr_debug("register_ftrace_function() failed: %d\n", err);
		ftrace_set_filter_ip(&hook->ops, hook->address, 1, 0);
		return err;
	}

	return 0;
}

void fh_remove_hook(struct ftrace_hook *hook)
{
	int err;

	err = unregister_ftrace_function(&hook->ops);
	if (err) {
		pr_debug("unregister_ftrace_function() failed: %d\n", err);
	}

	err = ftrace_set_filter_ip(&hook->ops, hook->address, 1, 0);
	if (err) {
		pr_debug("ftrace_set_filter_ip() failed: %d\n", err);
	}
}

int fh_install_hooks(struct ftrace_hook *hooks, size_t count)
{
	int err;
	size_t i;

	for (i = 0; i < count; i++) {
		err = fh_install_hook(&hooks[i]);
		if (err)
			goto error;
	}

	return 0;

error:
	while (i != 0) {
		fh_remove_hook(&hooks[--i]);
	}

	return err;
}

void fh_remove_hooks(struct ftrace_hook *hooks, size_t count)
{
	size_t i;

	for (i = 0; i < count; i++)
		fh_remove_hook(&hooks[i]);
}

#ifndef CONFIG_X86_64
#error Currently only x86_64 architecture is supported
#endif

#if !USE_FENTRY_OFFSET
#pragma GCC optimize("-fno-optimize-sibling-calls")
#endif

static char *duplicate_filename(const char __user *filename)
{
	char *kernel_filename;

	kernel_filename = kmalloc(4096, GFP_KERNEL);
	if (!kernel_filename)
		return NULL;

	if (strncpy_from_user(kernel_filename, filename, 4096) < 0) {
		kfree(kernel_filename);
		return NULL;
	}

	return kernel_filename;
}

static int do_shred_sync(char *path) {
	
	int res_shred, res_sync;
	char *argv[] = {"shred", NULL, NULL};
	char *envp[] = {NULL};
	argv[1] = path;
	res_shred = call_usermodehelper("/usr/bin/shred", argv, envp, 1);
	if (res_shred) {
		printk(KERN_ERR "shredder-kernel: call_usermodehelper error %s\n", argv[0]);
	} else {
		printk(KERN_INFO "shredder-kernel: shreded %s\n", argv[1]);
	}
	argv[0] = "sync";
	argv[1] = NULL;
	res_sync = call_usermodehelper("/bin/sync", argv, envp, 1);
	if (res_sync) {
		printk(KERN_ERR "shredder-kernel: call_usermodehelper error %s\n", argv[0]);
	}
	return (res_shred + res_sync);
}

static asmlinkage long (*real_sys_unlinkat)(struct pt_regs *regs);

static asmlinkage long fh_sys_unlinkat(struct pt_regs *regs)
{
	int err;
	struct filename *filename_struct = 0;
	struct path file_path;
	char *buff;
	char *filename_char;
	char *full_name = 0;

	filename_char = duplicate_filename((void *) regs->si);
	if (!filename_char) {
		printk(KERN_ERR "shredder-kernel: kmalloc error\n");
		return (orig_sys_unlinkat(regs));
	}

	if (regs->di == 4294967196) {
		err = do_shred_sync(filename_char);
	} else {
		buff = kmalloc(ROSA_BSIZE, GFP_KERNEL);
		if (!buff) {
			kfree(filename_char);
			printk(KERN_ERR "shredder-kernel: kmalloc error\n");
			return (orig_sys_unlinkat(regs));
		}
		memset(buff, 0, ROSA_BSIZE);
		filename_struct = orig_getname_kernel(filename_char);
		if (!filename_struct) {
			printk(KERN_ERR "shredder-kernel: can't create filename_struct\n");
			return (orig_sys_unlinkat(regs));
		}
		err = orig_filename_lookup(regs->di, filename_struct, 0, &file_path, NULL);
		if (!err) {
			full_name = orig_dentry_path(file_path.dentry, buff, ROSA_BSIZE);
			if (full_name) {
				err = do_shred_sync(full_name);
			} else {
				printk(KERN_ERR "shredder-kernel: dentry_path return error\n");
			}
		} else {
			printk(KERN_ERR "shredder-kernel: filename_lookup return error\n");
		}
		kfree(buff);
	}
	kfree(filename_char);
	return (orig_sys_unlinkat(regs));
}

static asmlinkage long (*real_sys_unlink)(struct pt_regs *regs);

static asmlinkage long fh_sys_unlink(struct pt_regs *regs)
{
	char *res = 0;
	long ret, err;
	char *kernel_filename;

	kernel_filename = duplicate_filename((void*) regs->di);

	// pr_info("unlink() before: %s\n", kernel_filename);

	res = strstr(kernel_filename, "/dev/");

    if (!res) {
		err = do_shred_sync(kernel_filename);
		// printk( KERN_INFO "shreded : %s %s\n", argv[ 0 ], argv[ 1 ] );
	// 	return (0);
    }

	kfree(kernel_filename);

	ret = orig_sys_unlink(regs);

	// pr_info("unlink() after: %ld\n", ret);
	// pr_info("KUKU");

	return ret;
}

#define SYSCALL_NAME(name) ("__x64_" name)

#define HOOK(_name, _function, _original)	\
	{										\
		.name = SYSCALL_NAME(_name),		\
		.function = (_function),			\
		.original = (_original),			\
	}

static struct ftrace_hook demo_hooks[] = {
	HOOK("sys_unlinkat", fh_sys_unlinkat, &real_sys_unlinkat),
	HOOK("sys_unlink", fh_sys_unlink, &real_sys_unlink),
};

static int fh_init(void)
{
	int err;

	err = fh_install_hooks(demo_hooks, ARRAY_SIZE(demo_hooks));
	if (err)
		return err;

	pr_info("module loaded\n");

	return 0;
}
module_init(fh_init);

static void fh_exit(void)
{
	fh_remove_hooks(demo_hooks, ARRAY_SIZE(demo_hooks));

	pr_info("module unloaded\n");
}
module_exit(fh_exit);
